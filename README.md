# Alumni by LOG (Deprecated)

[![build status](https://gitlab.com/logbruchkoebel/alumni/badges/master/build.svg)](https://gitlab.com/logbruchkoebel/alumni/builds)

**This project is no longer maintained!**

With Alumni by LOG you can provide your alumnis a possibility to get
regular updates via newsletters. You can easily deploy it on your web
server if it can handle PHP and MySQL.

It was built in schools and made open source with the result that every
school in the world can make use of it, study and modify it for their
needs. With using this piece of software, you make sure that you have a
solution that is customized for your needs and runs far in the future,
without worrying about licenses.

## License

This project is licensed under the GPLv3 license. For more information,
see [LICENSE](./LICENSE).

## Credits

The logo was made by [Freepik](http://www.freepik.com) from
[flaticon](http://www.flaticon.com) and is licensed by
[CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/).
