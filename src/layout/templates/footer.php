<?php
/*
 * @author	Nico Alt
 * @date	21.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
?>
			</div>
		</section>

		<!-- footer area -->
		<footer>
			<div id="colophon" class="wrapper clearfix" align="center">
				<?=$footer_title?>
			</div>
			<div id="colophon" class="wrapper clearfix" align="center">
				Version 0.1.0 &copy; Nico Alt
			</div>
			<div id="attribution" class="wrapper clearfix" style="color:#666; font-size:9px;" align="center">
				Site built with <a href="http://www.prowebdesign.ro/simple-responsive-template/" target="_blank" style="color:#777;">Simple Responsive Template</a>
			</div>
		</footer>

		<!-- jQuery -->
		<script>
			window.jQuery || document.write('<script src="<?=$goBack?>layout/js/libs/jquery.min.js">\x3C/script>')
		</script>

		<!-- fire ups - read this file!  -->
		<script src="<?=$goBack?>layout/js/main.js"></script>
		<script language="javascript" type="text/javascript" src="<?=$goBack?>layout/js/responsiveTable.js"></script>

	</body>
</html>
