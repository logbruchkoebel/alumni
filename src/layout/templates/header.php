<?php
/*
 * @author	Nico Alt
 * @date	21.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?=$title?></title>
		<!-- Mobile viewport -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
		<link rel="shortcut icon" href="<?=$goBack?>layout/media/favicon.png" type="image/x-icon">

		<!-- CSS-->
		<link rel="stylesheet" href="<?=$goBack?>layout/css/normalize.css">
		<link rel="stylesheet" href="<?=$goBack?>layout/css/style.css">
	</head>

	<body id="home">
		<!-- header area -->
		<header class="wrapper clearfix">
			<div id="banner">
				<div id="logo">
					<a href="index.php"><img src="<?=$goBack?>layout/media/logo.png" alt="logo"></a>
				</div>
			</div>

			<!-- main navigation -->
			<nav id="topnav" role="navigation">
				<div class="menu-toggle">Menü</div>
				<ul class="srt-menu" id="menu-main-navigation">
					<?=$navigation?>
				</ul>
			</nav>
		</header>

		<!-- hero area (the grey one with a slider -->
		<section id="page-header" class="clearfix">
			<div class="wrapper" align="center">
				<h1><?=$title?></h1>
			</div>
		</section>

		<section id="features" class="greenelement vertical-padding">
			<div class="wrapper clearfix" align="center">
