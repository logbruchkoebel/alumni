<?php
/*
 * @author	Nico Alt
 * @date	21.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
?>
<li<? if ($current == 1) echo " class=\"current\"" ?>><a href="index.php">Zentrale</a></li>
<li<? if ($current == 2) echo " class=\"current\"" ?>><a href="write.php">Schreiben</a></li>
<li<? if ($current == 3) echo " class=\"current\"" ?>><a href="search.php">Suchen</a></li>
<li<? if ($current == 4) echo " class=\"current\"" ?>><a href="update.php">Aktualisieren</a></li>
<li<? if ($current == 5) echo " class=\"current\"" ?>><a href="show.php">Anzeigen</a></li>
<li<? if ($current == 6) echo " class=\"current\"" ?>><a href="signin.php">Eintragen</a></li>
<li<? if ($current == 7) echo " class=\"current\"" ?>><a href="signout.php">Austragen</a></li>
