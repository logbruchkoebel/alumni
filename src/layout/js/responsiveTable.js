  /**
   * Responsive HTML Table
   *
   * @desc RWD: HTML table turns into an accordion.
   * @author [HZ]
   * @dependency jQuery
   *
   * source: http://www.freizeitler.de/2014/08/19/usability-und-content-strategien-eine-tabelle-im-responsive-web-design/
   */
  $.fn.responsiveTable = function () {
      $(this).find('table').each(function () {
          var trAcc = $(this).find('tr').not('tr:first-child'),
              thHead = [];

          $(this).find('tr:first-child td, tr:first-child th').each(function () {
              headLines = $(this).text();
              thHead.push(headLines);
          });

          trAcc.each(function () {
              for (i = 0, l = thHead.length; i < l; i++) {
                  $(this).find('td').eq(i + 1).prepend('<h3>' + thHead[i + 1] + '</h3>');
              }
          });

          trAcc.append('<i class="icon-accordion">+</i>');
          trAcc.click(function () {
              if ($(this).hasClass('accordion-opened')) {
                  $(this).animate({
                      maxHeight: '60px'
                  }, 200).removeClass('accordion-opened').find('.icon-accordion').text('+');

              } else {
                  $(this).animate({
                      maxHeight: '1000px'
                  }, 400).addClass('accordion-opened').find('.icon-accordion').text('-');
              }
          });
      });
  };

  // Init
  $(function () {
      $('.js-responsive-table').responsiveTable();
  });
