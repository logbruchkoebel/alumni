<?php
/*
 * @author	Nico Alt
 * @date	21.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
// Show header
require '../lib/layout.php';
$layout = new Layout();
echo $layout->header("", 1, true, "../");
?>
<h2>Was willst Du tun?</h2>
<h3><a href="write.php">Einen Newsletter verfassen</a></h3>
<h3><a href="search.php">Einen Eintrag suchen</a></h3>
<h3><a href="update.php">Einen Eintrag aktualisieren</a></h3>
<h3><a href="show.php">Alle Einträge anzeigen</a></h3>
<h3><a href="signin.php">Jemanden ohne Bestätigungsmail eintragen</a></h3>
<h3><a href="signout.php">Jemanden ohne Bestätigungsmail austragen</a></h3>
<?
// Show footer
echo $layout->footer("../");
?>
