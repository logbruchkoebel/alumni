<?php
/*
 * @author	Nico Alt
 * @date	21.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
$token = $_GET['t'];

try {
	require '../lib/confirmation.php';
	$confirmation = new Confirmation();

	// Confirm action
	$value = $confirmation->confirm_Admin($token);
	switch ($value) {
		case 1:
			require '../lib/newsletter.php';
			$newsletter = new Newsletter();
			if (!$newsletter->send($token)) {
				throw new Exception("Der Newsletter konnte nicht versandt werden. Bitte kontaktiere den Admin.");
			}
			$msg = "Der Newsletter wurde erfolgreich versandt.";
			break;
		default:
			throw new Exception("Es gab einen Fehler. Bitte kontaktiere den Admin.");
	}
}
catch (Exception $e) {
	$msg = $e->getMessage();
}

// Show header
require '../lib/layout.php';
$layout = new Layout();
echo $layout->header("Bestätigung", 0, true, "../");
?>
<p><b><?=$msg?></b></p>
<?
// Show footer
echo $layout->footer("../");
?>
