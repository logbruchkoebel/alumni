<?php
/*
 * @author	Nico Alt
 * @date	21.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
$lastname = $_POST['lastname'];
$firstname = $_POST['firstname'];
$mail = $_POST['mail'];
$agegroup = $_POST['agegroup'];
$birthday = $_POST['birthday'];
$role = $_POST['role'];
$status = $_POST['status'];
$birthname = $_POST['birthname'];

try {
	// Check if strings are "POSTed"
	if (empty($lastname) && empty($firstname) && empty($mail) && empty($agegroup) && empty($birthday) && empty($role) && !isset($status) && empty($birthname)) {
		throw new Exception();
	}

	require '../lib/database.php';
	$database = new Database();

	// Parse entries
	$query = $database->search($lastname, $firstname, $mail, $agegroup, $birthday, $role, $status, $birthname, false);
	$divider = "</td>\n<td align=\"center\">";
	while($column = mysqli_fetch_array($query)) {
		$mailTable = $column['mail'] . $divider;
		if ($column['status'] != "0") {
			$link = "update.php?mail=" . $column['mail'];
			$mailTable = "<a href=\"" . $link . "\">" . $column['mail'] . "</a>" . $divider;
		}
		$table .= "<tr align=\"center\">\n<td align=\"center\">";
		$table .= $column['lastname'] . $divider;
		$table .= $column['firstname'] . $divider;
		$table .= $mailTable;
		$table .= $column['agegroup'] . $divider;
		$table .= $column['birthday'] . $divider;
		$table .= $column['role'] . $divider;
		$table .= $column['edited'] . $divider;
		$table .= $column['added'] . $divider;
		$table .= $column['status'] . $divider;
		$table .= $column['token'] . $divider;
		$table .= $column['birthname'] . $divider;
	}
}
catch (Exception $e) {
	$msg = $e->getMessage();
}

// Show header
require '../lib/layout.php';
$layout = new Layout();
echo $layout->header("Suchen", 3, true, "../");
?>
<p><b><?=$msg?></b></p>
<form method="post">
	<p><input type="text" name="lastname" placeholder="Nachname" value="<?=$lastname?>"/></p>
	<p><input type="text" name="firstname" placeholder="Vorname" value="<?=$firstname?>"/></p>
	<p><input type="text" name="mail" placeholder="E-Mail-Adresse" value="<?=$mail?>"/></p>
	<p><input type="number" name="agegroup" placeholder="Jahrgang" value="<?=$agegroup?>" min="1974" max="2030" step="1"/></p>
	<p><input type="text" name="birthday" placeholder="Geburtstag (z.B. 1990-12-31)" value="<?=$birthday?>"/></p>
	<p><input type="text" name="role" placeholder="Rolle (z.B. pupil)" value="<?=$role?>"/></p>
	<p><input type="number" name="status" placeholder="Status (z.B. 2)" value="<?=$status?>" min="0" max="20" step="1"/></p>
	<p><input type="text" name="birthname" placeholder="Geburtsname" value="<?=$birthname?>"/></p>
	<p><input type="submit"/></p>
</form>
<div class="js-responsive-table" align="center">
	<table align="center">
		<tr align="center">
			<th align="center">Nachname</th>
			<th align="center">Vorname</th>
			<th align="center">E-Mail Adresse</th>
			<th align="center">Jahrgang</th>
			<th align="center">Geburtstag</th>
			<th align="center">Rolle</th>
			<th align="center">Zuletzt geändert</th>
			<th align="center">Angelegt</th>
			<th align="center">Status</th>
			<th align="center">Token</th>
			<th align="center">Geburtsname</th>
		</tr>
		<?=$table?>
	</table>
</div>
<?
// Show footer
echo $layout->footer("../");
?>
