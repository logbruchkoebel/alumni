<?php
/*
 * @author	Nico Alt
 * @date	21.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
$subject = $_POST['subject'];
$mail = $_POST['mail'];
$message = $_POST['message'];

try {
	// Check if strings are "POSTed"
	if (empty($subject) && empty($mail) && empty($message)) {
		throw new Exception();
	}

	// Check if strings are not empty
	if ($subject == '' || $mail == '' || $message == '') {
		throw new Exception("Oh, da hast Du anscheinend was vergessen... Bitte überprüfe, ob Du alle Felder ausgefült haben.");
	}

	require '../lib/utilities.php';
	$utils = new Utilities();

	// Check if mail is valid
	if (!$utils->checkMail($mail)) {
		throw new Exception("Nein, das geht nicht. Die eingegebene E-Mail-Adresse existiert nicht.");
	}

	require '../lib/newsletter.php';
	$newsletter = new Newsletter();

	// Send newsletter test
	if (!$newsletter->sendTest($subject, $mail, $message)) {
		throw new Exception("Der Newsletter konnte nicht zum Test versandt werden. Bitte kontaktiere den Admin.");
	}

	// Print out message
	$msg = "Der Newsletter wurde erfolgreich zum Test versandt.";
}
catch (Exception $e) {
	$msg = $e->getMessage();
}

// Show header
require '../lib/layout.php';
$layout = new Layout();
echo $layout->header("Schreiben", 2, true, "../");
?>
<p><b><?=$msg?></b></p>
<form action="#" method="POST">
	<p><input type="text" name="subject" placeholder="Betreff *" value="<?=$subject?>" size="64"/></p>
	<p><input type="text" name="mail" placeholder="Addresse für Newsletter-Test *" value="<?=$mail?>" size="64"/></p>
	<p><textarea type="text" name="message" placeholder="Nachricht *" cols="64" rows="25"><?=$message?></textarea></p>
	<p>
		<input type="submit" value="Absenden">
		<input type="reset" value="Eingaben löschen">
	</p>
</form>
<?
// Show footer
echo $layout->footer("../");
?>
