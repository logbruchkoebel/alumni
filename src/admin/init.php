<?php
/*
 * @author	Nico Alt
 * @date	21.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
class Setup {

	// Configuration
	private $config;

	// Connection to database
	private $db;

	// Table name Alumni
	private $table_alumni;

	// Table name Newsletter
	private $table_newsletter;

	/**
	 * Connect with the database.
	 */
	public function __construct() {
		$this->config = parse_ini_file(__DIR__ . "/../lib/config/configuration.ini", true);
		$config_mysql = $this->config["MySQL"];
		$mysqlhost = $config_mysql["Host"];
		$mysqluser = $config_mysql["User"];
		$mysqlpw = $config_mysql["Password"];
		$mysqldb = $config_mysql["Database"];
		$this->table_alumni = $config_mysql["Table_Alumni"];
		$this->table_newsletter = $config_mysql["Table_Newsletter"];
		$this->db = new mysqli($mysqlhost, $mysqluser, $mysqlpw, $mysqldb);
	}

	/**
	 * Create table alumni
	 */
	public function createAlumni() {
		$sql = "CREATE TABLE IF NOT EXISTS `" . $this->table_alumni . "` (
		  firstname VARCHAR(300) NOT NULL,
		  lastname VARCHAR(300) NOT NULL,
		  mail VARCHAR(255) NOT NULL,
		  agegroup YEAR DEFAULT NULL,
		  birthday DATE DEFAULT NULL,
		  role VARCHAR(10) DEFAULT 'pupil',
		  added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		  edited TIMESTAMP,
		  status CHAR(1) DEFAULT '3',
		  token VARCHAR(32) DEFAULT NULL,
		  birthname VARCHAR(300) DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		return $this->db->query($sql);
	}

	/**
	 * Drop table alumni
	 */
	public function dropAlumni() {
		if (!$this->backupAlumni()) {
			return false;
		}
		$sql = "DROP TABLE IF EXISTS " . $this->table_alumni . "";
		return $this->db->query($sql);
	}

	/**
	 * Backup table alumni
	 */
	private function backupAlumni() {
		$sql = "SELECT 1 FROM " . $this->table_alumni . " LIMIT 1";
		if (!$this->db->query($sql)) {
			return false;
		}
		$sql = "DROP TABLE IF EXISTS " . $this->table_alumni . "_backup";
		$this->db->query($sql);
		$sql = "CREATE TABLE " . $this->table_alumni . "_backup LIKE " . $this->table_alumni . "";
		$this->db->query($sql);
		$sql = "INSERT " . $this->table_alumni . "_backup SELECT * FROM " . $this->table_alumni . "";
		return $this->db->query($sql);
	}

	/**
	 * Reset table alumni
	 */
	public function resetAlumni() {
		$sql = "DROP TABLE IF EXISTS " . $this->table_alumni . "";
		$this->db->query($sql);
		$sql = "CREATE TABLE " . $this->table_alumni . " LIKE " . $this->table_alumni . "_backup";
		$this->db->query($sql);
		$sql = "INSERT " . $this->table_alumni . " SELECT * FROM " . $this->table_alumni . "_backup";
		return $this->db->query($sql);
	}

	/**
	 * Create table newsletter
	 */
	public function createNewsletter() {
		$sql = "CREATE TABLE IF NOT EXISTS `" . $this->table_newsletter . "` (
		  token VARCHAR(32) NOT NULL,
		  subject TEXT NOT NULL,
		  newsletter LONGTEXT NOT NULL,
		  log LONGTEXT,
		  sent BOOLEAN DEFAULT 0
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		return $this->db->query($sql);
	}

	/**
	 * Drop table newsletter
	 */
	public function dropNewsletter() {
		if (!$this->backupNewsletter()) {
			return false;
		}
		$sql = "DROP TABLE IF EXISTS " . $this->table_newsletter . "";
		return $this->db->query($sql);
	}

	/**
	 * Backup table newsletter
	 */
	private function backupNewsletter() {
		$sql = "SELECT 1 FROM " . $this->table_newsletter . " LIMIT 1";
		if (!$this->db->query($sql)) {
			return false;
		}
		$sql = "DROP TABLE IF EXISTS " . $this->table_newsletter . "_backup";
		$this->db->query($sql);
		$sql = "CREATE TABLE " . $this->table_newsletter . "_backup LIKE " . $this->table_newsletter . "";
		$this->db->query($sql);
		$sql = "INSERT " . $this->table_newsletter . "_backup SELECT * FROM " . $this->table_newsletter . "";
		return $this->db->query($sql);
	}

	/**
	 * Reset table newsletter
	 */
	public function resetNewsletter() {
		$sql = "DROP TABLE IF EXISTS " . $this->table_newsletter . "";
		$this->db->query($sql);
		$sql = "CREATE TABLE " . $this->table_newsletter . " LIKE " . $this->table_newsletter . "_backup";
		$this->db->query($sql);
		$sql = "INSERT " . $this->table_newsletter . " SELECT * FROM " . $this->table_newsletter . "_backup";
		return $this->db->query($sql);
	}

	/**
	 * Return MySQL version
	 */
	public function getMysqlVersion() {
		return $this->db->server_version;
	}
}

// Include Setup
$setup = new Setup();

// Show header
require '../lib/layout.php';
$layout = new Layout();
echo $layout->header("Setup", 0, true, "../");

// Get strings
$createAlumni = $_POST["createAlumni"];
$createNewsletter = $_POST["createNewsletter"];
$dropAlumni = $_POST["dropAlumni"];
$dropNewsletter = $_POST["dropNewsletter"];
$resetAlumni = $_POST["resetAlumni"];
$resetNewsletter = $_POST["resetNewsletter"];

// Show MySQL Version
echo "<p>MySQL Version: " . $setup->getMysqlVersion() . "</p>";

try {
	if ($createAlumni != '') {
		if ($setup->createAlumni()) {
			throw new Exception("Table <i>alumni</i> now exists.");
		}
		throw new Exception("Error creating table <i>alumni</i>.");
	}
	if ($createNewsletter != '') {
		if ($setup->createNewsletter()) {
			throw new Exception("Table <i>newsletter</i> now exists.");
		}
		throw new Exception("Error creating table <i>newsletter</i>.");
	}
	if ($dropAlumni != '') {
		if ($setup->dropAlumni()) {
			throw new Exception("Table <i>alumni</i> dropped.");
		}
		throw new Exception("Error dropping table <i>alumni</i>.");
	}
	if ($dropNewsletter != '') {
		if ($setup->dropNewsletter()) {
			throw new Exception("Table <i>newsletter</i> dropped.");
		}
		throw new Exception("Error dropping table <i>newsletter</i>.");
	}
	if ($resetAlumni != '') {
		if ($setup->resetAlumni()) {
			throw new Exception("Table <i>alumni</i> reset.");
		}
		throw new Exception("Error resetting table <i>alumni</i>.");
	}
	if ($resetNewsletter != '') {
		if ($setup->resetNewsletter()) {
			throw new Exception("Table <i>newsletter</i> reset.");
		}
		throw new Exception("Error resetting table <i>newsletter</i>.");
	}
}
catch (Exception $e) {
	$msg = $e->getMessage();
}
?>
<p><b><?=$msg?></b></p>
<p>Was willst Du tun?</p>
<form method="post">
	<p><input type="submit" name="createAlumni" value="Create table alumni"/></p>
	<p><input type="submit" name="createNewsletter" value="Create table newsletter"/></p>
	<p><input type="submit" name="dropAlumni" value="Drop table alumni"/></p>
	<p><input type="submit" name="dropNewsletter" value="Drop table newsletter"/></p>
	<p><input type="submit" name="resetAlumni" value="Reset table alumni"/></p>
	<p><input type="submit" name="resetNewsletter" value="Reset table newsletter"/></p>
</form>
<?
// Show footer
echo $layout->footer("../");
?>
