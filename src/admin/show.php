<?php
/*
 * @author	Nico Alt
 * @date	21.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
try {
	require '../lib/database.php';
	$database = new Database();

	// Parse entries
	$query = $database->getAllEntries();
	$entriesCount = $query->num_rows;
	if ($entriesCount == 0) {
		throw new Exception("Es wurden keine Einträge gefunden.");
	}
	$msg = sprintf("Es wurden %d Einträge gefunden.", $entriesCount);
	$divider = "</td>\n<td align=\"center\">";
	while($column = mysqli_fetch_array($query)) {
		$mailTable = $column['mail'] . $divider;
		if ($column['status'] != "0") {
			$link = "update.php?mail=" . $column['mail'];
			$mailTable = "<a href=\"" . $link . "\">" . $column['mail'] . "</a>" . $divider;
		}
		$table .= "<tr align=\"center\">\n<td align=\"center\">";
		$table .= $column['lastname'] . $divider;
		$table .= $column['firstname'] . $divider;
		$table .= $mailTable;
		$table .= $column['agegroup'] . $divider;
		$table .= $column['birthday'] . $divider;
		$table .= $column['role'] . $divider;
		$table .= $column['edited'] . $divider;
		$table .= $column['added'] . $divider;
		$table .= $column['status'] . $divider;
		$table .= $column['token'] . $divider;
		$table .= $column['birthname'] . $divider;
	}
}
catch (Exception $e) {
	$msg = $e->getMessage();
}

// Show header
require '../lib/layout.php';
$layout = new Layout();
echo $layout->header("Anzeigen", 5, true, "../");
?>
<p><b><?=$msg?></b></p>
<div class="js-responsive-table" align="center">
	<table align="center">
		<tr align="center">
			<th align="center">Nachname</th>
			<th align="center">Vorname</th>
			<th align="center">E-Mail Adresse</th>
			<th align="center">Jahrgang</th>
			<th align="center">Geburtstag</th>
			<th align="center">Rolle</th>
			<th align="center">Zuletzt geändert</th>
			<th align="center">Angelegt</th>
			<th align="center">Status</th>
			<th align="center">Token</th>
			<th align="center">Geburtsname</th>
		</tr>
		<?=$table?>
	</table>
</div>
<?
// Show footer
echo $layout->footer("../");
?>
