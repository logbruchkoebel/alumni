<?php
/*
 * @author	Nico Alt
 * @date	21.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
$lastname = $_POST['lastname'];
$firstname = $_POST['firstname'];
$mail = $_POST['mail'];
$agegroup = $_POST['agegroup'];
$birthday = $_POST['birthday'];
$role = $_POST['role'];
$status = $_POST['status'];
$birthname = $_POST['birthname'];

$getMail = $_GET['mail'];
$getStatus = $_GET['status'];
try {
	// Check if strings are "POSTed"
	if (empty($lastname) && empty($firstname) && empty($mail) && empty($agegroup) && empty($birthday) && empty($role) && empty($status) && empty($birthname)) {
		throw new Exception();
	}

	// Check if strings are not empty
	if ($mail == '') {
		throw new Exception("Du musst eine E-Mail-Adresse zur Identifikation eingeben.");
	}

	require '../lib/utilities.php';
	$utils = new Utilities();

	// Check if mail is valid
	if (!$utils->checkMail($mail)) {
		throw new Exception("Nein, das geht nicht. Die eingegebene E-Mail-Adresse existiert nicht.");
	}

	require '../lib/database.php';
	$database = new Database();

	// Check that entry does not exist already
	if (!$database->checkDouble($mail)) {
		throw new Exception("Die eingegebene E-Mail-Adresse ist nicht eingetragen.");
	}

	// Update entry
	if (!$database->updateEntry($mail, $lastname, $firstname, $agegroup, $birthday, $role, $status, $birthname)) {
		throw new Exception("Der Eintrag konnte nicht aktualisiert werden. Bitte kontaktiere den Admin.");
	}

	// Print out message
	$msg = "Der Eintrag mit der E-Mail-Adresse <i>'$mail'</i> wurde erfolgreich aktualisiert.";
}
catch (Exception $e) {
	$msg = $e->getMessage();
}

// Check if a mail was sent by GET
if ($getMail != '') {
	$query = $database->search("", "", $getMail, "", "", "", "", "", true);
	$column = mysqli_fetch_array($query);
	$lastname = $column['lastname'];
	$firstname = $column['firstname'];
	if ($column['agegroup'] != '0') {
		$agegroup = $column['agegroup'];
	}
	if ($column['birthday'] != '0000-00-00') {
		$birthday = $column['birthday'];
	}
	$role = $column['role'];
	$status = $column['status'];
	$birthname = $column['birthname'];
	$mail = $getMail;
}

// Show header
require '../lib/layout.php';
$layout = new Layout();
echo $layout->header("Aktualisieren", 4, true, "../");
?>
<p><b><?=$msg?></b></p>
<p>Die Identifikation des Datensatzes erfolgt durch die E-Mail Adresse.</p>
<p>Bei leeren Feldern wird der vorherige Wert genommen, die Daten werden nicht gelöscht.</p>
<form method="post" accept-charset="UTF-8">
	<p><input type="text" name="mail" placeholder="E-Mail *" value="<?=$mail?>"/></p>
	<p><input type="text" name="lastname" placeholder="Name" value="<?=$lastname?>"/></p>
	<p><input type="text" name="firstname" placeholder="Vorname" value="<?=$firstname?>"/></p>
	<p><input type="number" name="agegroup" placeholder="Jahrgang" value="<?=$agegroup?>" min="1974" max="2030" step="1"/></p>
	<p><input type="text" name="birthday" placeholder="Geburtstag (z.B. 1990-12-31)" value="<?=$birthday?>"/></p>
	<p><input type="text" name="role" placeholder="Rolle (z.B. pupil)" value="<?=$role?>"/></p>
	<p><input type="number" name="status" placeholder="Status (z.B. 2)" value="<?=$status?>" min="0" max="20" step="1"/></p>
	<p><input type="text" name="birthname" placeholder="Geburtsname" value="<?=$birthname?>"/></p>
	<p><input type="submit" /></p>
</form>
<?
// Show footer
echo $layout->footer("../");
?>
