<?php
/*
 * @author	Nico Alt
 * @date	21.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
require 'lib/configuration.php';

$title_school = Configuration::get("General", "Title_School");

// Show header
require 'lib/layout.php';
$layout = new Layout();
echo $layout->header("", 1, false, "");
?>
<p><b>
	Herzlich willkommen im Alumni-Portal der Schule "<?=$title_school?>".
</b></p>
<h2>Was willst Du tun?</h2>
<h3><a href="signin.php">Ich möchte mich eintragen</a></h3>
<h3><a href="signout.php">Ich möchte mich austragen</a></h3>
<h3><a href="contact.php">Ich möchte das Alumni-Team kontaktieren</a></h3>
<?
// Show footer
echo $layout->footer("");
?>
