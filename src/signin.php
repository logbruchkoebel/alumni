<?php
/*
 * @author	Nico Alt
 * @date	21.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
$lastname = $_POST['lastname'];
$firstname = $_POST['firstname'];
$mail = $_POST['mail'];
$birthname = $_POST['birthname'];
$birthday = $_POST['birthday'];
$agegroup = $_POST['agegroup'];

try {
	// Check if strings are "POSTed"
	if (empty($lastname) && empty($firstname) && empty($mail) && empty($birthname) && empty($birthday) && empty($agegroup)) {
		throw new Exception();
	}

	// Check if strings are not empty
	if ($lastname == '' || $firstname == '' || $mail == '') {
		throw new Exception("Oh, da hast Du anscheinend was vergessen... Bitte überprüfe, ob Du alle Felder ausgefült haben.");
	}

	require 'lib/utilities.php';
	$utils = new Utilities();

	// Check if mail is valid
	if (!$utils->checkMail($mail)) {
		throw new Exception("Nein, das geht nicht. Die eingegebene E-Mail-Adresse existiert nicht.");
	}

	require 'lib/database.php';
	$database = new Database();

	// Check that entry does not exist already
	if ($database->checkDouble($mail)) {
		throw new Exception("Ohje, die eingegebene E-Mail-Adresse ist bereits eingetragen.");
	}

	// Sign in entry
	if (!$database->signIn($firstname, $lastname, $mail, $birthname, $birthday, $agegroup, false)) {
		throw new Exception("Ich kann Dich gerade nicht eintragen. Bitte versuche es später erneut.");
	}

	require 'lib/confirmation.php';
	$confirmation = new Confirmation();

	// Send confirmation mail
	if (!$confirmation->send_User($mail)) {
		$database->signOut($mail, false);
		throw new Exception("Die Bestätigungsmail konnte nicht abgesandt werden. Bitte versuche es später erneut.");
	}

	// Print out message with details
	$msg = "Alles klar, wir haben Dir eine Bestätigungsmail an die Adresse <i>\"$mail\"</i> geschickt. Bitte bestätige Deine Adresse.";
}
catch (Exception $e) {
	$msg = $e->getMessage();
}

// Show header
require 'lib/layout.php';
$layout = new Layout();
echo $layout->header("Eintragen", 2, false, "");
?>
<p><b><?=$msg?></b></p>
<p>Trage Dich in die Datenbank ein:</p>
<form method="post" accept-charset="UTF-8">
	<p><input type="text" name="firstname" placeholder="Vorname *" value="<?=$firstname?>"/></p>
	<p><input type="text" name="lastname" placeholder="Nachname *" value="<?=$lastname?>"/></p>
	<p><input type="email" name="mail" placeholder="E-Mail *" value="<?=$mail?>"/></p>
	<p><input type="text" name="birthname" placeholder="Geburtsname" value="<?=$birthname?>"/></p>
	<p><input type="text" name="birthday" placeholder="Geburtstag (z.B. 1990-12-31)" value="<?=$birthday?>"/></p>
	<p><input type="number" name="agegroup" placeholder="Jahrgang (z.B. 1993)" value="<?=$agegroup?>" min="1974" max="2030" step="1"/></p>
	<p><input type="submit" /></p>
</form>
<?
// Show footer
echo $layout->footer("");
?>
