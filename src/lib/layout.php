<?php
/*
 * @author	Nico Alt
 * @date	28.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
class Layout {

	/**
	 * Connect with the database.
	 */
	public function __construct() {
		require_once __DIR__ . '/configuration.php';
	}

	/**
	 * Return html template header
	 */
	function header($title, $current, $admin, $goBack) {
		ob_start();
		if ($title != "") {
			$title .= " - ";
		}
		if ($admin) {
			$title .= "Admin - ";
			require __DIR__ . "/../layout/templates/navigationAdmin.php";
			$navigation = ob_get_clean();
		}
		else {
			require __DIR__ . "/../layout/templates/navigationUser.php";
			$navigation = ob_get_clean();
		}
		$title .= Configuration::get("Website", "Title");
		ob_start();
		require __DIR__ . "/../layout/templates/header.php";
		return ob_get_clean();
	}

	/**
	 * Return html template footer
	 */
	function footer($goBack) {
		$footer_title = Configuration::get("Website", "Footer_Title");
		ob_start();
		require __DIR__ . "/../layout/templates/footer.php";
		return ob_get_clean();
	}
}
?>
