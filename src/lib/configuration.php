<?php
/*
 * @author	Nico Alt
 * @date	28.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
class Configuration {

	/*
	 * Get a key in a group from the configuration.
	 */
	public static function get($group, $key) {
		$configuration = parse_ini_file(__DIR__ . "/config/configuration.ini", true);
		return $configuration[$group][$key];
	}
}
?>
