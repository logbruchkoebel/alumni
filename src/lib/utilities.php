<?php
/*
 * @author	Nico Alt
 * @date	21.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
class Utilities {

	/**
	 * Connect with the database.
	 */
	public function __construct() {
		require_once __DIR__ . '/configuration.php';
	}

	/**
	 * Validate an mail address.
	 * Returns true if the mail address is valid.
	 * Returns false if not.
	 */
	public static function checkMail($mail) {
	   $isValid = true;
	   $atIndex = strrpos($mail, "@");
	   if (is_bool($atIndex) && !$atIndex) {
		  $isValid = false;
	   }
	   else {
		  $domain = substr($mail, $atIndex+1);
		  $local = substr($mail, 0, $atIndex);
		  $localLen = strlen($local);
		  $domainLen = strlen($domain);
		  if ($localLen < 1 || $localLen > 64) {
			 // local part length exceeded
			 $isValid = false;
		  }
		  else if ($domainLen < 1 || $domainLen > 255) {
			 // domain part length exceeded
			 $isValid = false;
		  }
		  else if ($local[0] == '.' || $local[$localLen-1] == '.') {
			 // local part starts or ends with '.'
			 $isValid = false;
		  }
		  else if (preg_match('/\\.\\./', $local)) {
			 // local part has two consecutive dots
			 $isValid = false;
		  }
		  else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
			 // character not valid in domain part
			 $isValid = false;
		  }
		  else if (preg_match('/\\.\\./', $domain)) {
			 // domain part has two consecutive dots
			 $isValid = false;
		  }
		  else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local))) {
			 // character not valid in local part unless
			 // local part is quoted
			 if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local))) {
				$isValid = false;
			 }
		  }
		  if ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A"))) {
			 // domain not found in DNS
			 $isValid = false;
		  }
	   }
	   return $isValid;
	}

	/**
	 * Returns mail headers.
	 */
	public function getHeaders($sender_name = null, $sender_address = null, $sender_organization = null) {
		if ($sender_name == null) {
			$sender_name = Configuration::get("Confirmation", "Sender_Name");
		}
		if ($sender_address == null) {
			$sender_address = Configuration::get("Confirmation", "Sender_Address");
		}
		if ($sender_organization == null) {
			$sender_organization = Configuration::get("Confirmation", "Sender_Organization");
		}
		$headers = "Reply-To: " . $sender_name . " <" . $sender_address . ">\r\n";
		$headers .= "Return-Path: " . $sender_name . " <" . $sender_address . ">\r\n";
		$headers .= "From: " . $sender_name . " <" . $sender_address . ">\r\n";
		$headers .= "Organization: " . $sender_organization . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/plain; charset=UTF-8\r\n";
		$headers .= "X-Priority: 3\r\n";
		$headers .= "X-Mailer: PHP". phpversion() ."\r\n";
		return $headers;
	}
}
?>
