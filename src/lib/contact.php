<?php
/*
 * @author	Nico Alt
 * @date	28.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
class Contact {

	/**
	 * Connect with the database.
	 */
	public function __construct() {
		require_once __DIR__ . '/configuration.php';
	}

	/**
	 * Send a mail from the contact form.
	 */
	public function send_User($firstname, $lastname, $mail, $message) {
		$recipient_address = Configuration::get("Contact", "Recipient_Address");
		$subject = Configuration::get("Contact", "Subject");
		$message = 	"Eine neue Nachricht wurde über das Kontaktformular des Alumni-Projekts abgesendet:" . "\r\n" . "\r\n" .
					"Vorname: $firstname" . "\r\n" .
					"Nachname: $lastname" . "\r\n" .
					"E-Mail Adresse: $mail" . "\r\n" .
					"Nachricht:" . "\r\n" . "\r\n" .
					"$message";

		require_once __DIR__ . '/utilities.php';
		$utils = new Utilities();
		$headers = $utils->getHeaders(null, null, null);

		// Send mail
		return mail($recipient_address, $subject, $message, $headers);
	}
}
?>