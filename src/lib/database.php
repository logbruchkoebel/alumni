<?php
/*
 * @author	Nico Alt
 * @date	28.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
class Database {

	// Connection to database
	private $db;

	// Table name Alumni
	public static $table_alumni;

	// Table name Newsletter
	public static $table_newsletter;

	// Index numbers for statuses
	public static $status_dead = 0;
	public static $status_active = 1;
	public static $status_toGetConfirmed = 2;
	public static $status_toGetDeleted = 3;

	/**
	 * Connect with the database.
	 */
	public function __construct() {
		require_once __DIR__ . '/configuration.php';
		$mysqlhost = Configuration::get("MySQL", "Host");
		$mysqluser = Configuration::get("MySQL", "User");
		$mysqlpw = Configuration::get("MySQL", "Password");
		$mysqldb = Configuration::get("MySQL", "Database");
		self::$table_alumni = Configuration::get("MySQL", "Table_Alumni");
		self::$table_newsletter = Configuration::get("MySQL", "Table_Newsletter");
		$this->db = new mysqli($mysqlhost, $mysqluser, $mysqlpw, $mysqldb);
	}

	/**
	 * Get database connection.
	 */
	public function get() {
		return $this->db;
	}

	/**
	 * Sign in a person.
	 */
	public function signIn($firstname, $lastname, $mail, $birthname, $birthday, $agegroup, $force) {
		$firstname = $this->db->escape_string($firstname);
		$lastname = $this->db->escape_string($lastname);
		$mail = $this->db->escape_string($mail);
		$birthname = $this->db->escape_string($birthname);
		$birthday = $this->db->escape_string($birthday);
		$agegroup = $this->db->escape_string($agegroup);
		$token = bin2hex(openssl_random_pseudo_bytes(16));
		$sql = "INSERT INTO " . self::$table_alumni .
				" (
					lastname,
					firstname,
					mail,
					birthname,
					agegroup,
					birthday,
					token
				)" .
				" VALUES (
					'$lastname',
					'$firstname',
					'$mail',
					'$birthname',
					'$agegroup',
					'$birthday',
					'$token'
				);";
		if ($force) {
			$sql = "INSERT INTO " . self::$table_alumni .
					" (
						lastname,
						firstname,
						mail,
						birthname,
						agegroup,
						birthday,
						token,
						status
					)" .
					" VALUES (
						'$lastname',
						'$firstname',
						'$mail',
						'$birthname',
						'$agegroup',
						'$birthday',
						'$token',
						" . self::$status_active .
					")";
		}
		return $this->db->query($sql);
	}

	/**
	 * Resign in a person.
	 */
	public function resignIn($mail) {
		$mail = $this->db->escape_string($mail);
		$sql = "UPDATE " . self::$table_alumni .
				" SET" .
					" status = '" . self::$status_active .
				"' WHERE" .
					" mail LIKE '$mail' AND" .
					" status NOT LIKE " . self::$status_dead;
		return $this->db->query($sql);
	}

	/**
	 * Sign out a person.
	 */
	public function signOut($mail, $force) {
		$mail = $this->db->escape_string($mail);
		$sql = "SELECT status FROM " . self::$table_alumni .
				" WHERE" .
					" mail LIKE '$mail' AND" .
					" status NOT LIKE " . self::$status_dead .
				" LIMIT 1";
		$query = $this->db->query($sql)->fetch_array();
		$oldStatus = $query['status'];

		$status = self::$status_toGetDeleted;
		$token = bin2hex(openssl_random_pseudo_bytes(16));
		if ($oldStatus == self::$status_toGetConfirmed) {
			$status = self::$status_dead;
			$token = '';
		}
		if ($force) {
			$status = self::$status_dead;
		}
		$sql = "UPDATE " . self::$table_alumni .
				" SET" .
					" status = '$status'," .
					" token = '$token'" .
				" WHERE" .
					" mail LIKE '$mail' AND" .
					" status NOT LIKE " . self::$status_dead;
		return $this->db->query($sql);
	}

	/**
	 * Update a entry.
	 */
	public function updateEntry($mail, $lastname, $firstname, $agegroup, $birthday, $role, $status, $birthname) {
		$mail = $this->db->escape_string($mail);
		$lastname = $this->db->escape_string($lastname);
		$firstname = $this->db->escape_string($firstname);
		$agegroup = $this->db->escape_string($agegroup);
		$birthday = $this->db->escape_string($birthday);
		$role = $this->db->escape_string($role);
		$status = $this->db->escape_string($status);
		$birthname = $this->db->escape_string($birthname);
		if ($mail == '') {
			return false;
		}
		$updateScript = "SET ";
		if ($lastname != '') {
			$updateScript .= "lastname = '$lastname',";
		}
		if ($firstname != '') {
			$updateScript .= "firstname = '$firstname',";
		}
		if ($agegroup != '') {
			$updateScript .= "agegroup = '$agegroup',";
		}
		if ($birthday != '') {
			$updateScript .= "birthday = '$birthday',";
		}
		if ($role != '') {
			$updateScript .= "role = '$role',";
		}
		if ($status != '') {
			$updateScript .= "status = '$status',";
		}
		if ($birthname != '') {
			$updateScript .= "birthname = '$birthname',";
		}
		if ($updateScript == "SET ") {
			return false;
		}
		$updateScript = rtrim($updateScript, ",");
		$sql = "UPDATE " . self::$table_alumni . " " . $updateScript .
				" WHERE" .
					" mail LIKE '$mail' AND" .
					" status NOT LIKE " . self::$status_dead;
		return $this->db->query($sql);
	}

	/**
	 * Get all entries.
	 */
	public function getAllEntries() {
		$sql = "SELECT * FROM " . self::$table_alumni . " ORDER BY lastname ASC";
		return $this->db->query($sql);
	}

	/**
	 * Get all entries with the given lastname.
	 */
	public function search($lastname, $firstname, $mail, $agegroup, $birthday, $role, $status, $birthname, $notNull) {
		$lastname = "%" . $this->db->escape_string($lastname) . "%";
		$firstname = "%" . $this->db->escape_string($firstname) . "%";
		$mail = "%" . $this->db->escape_string($mail) . "%";
		$agegroup = "%" . $this->db->escape_string($agegroup) . "%";
		$birthday = "%" . $this->db->escape_string($birthday) . "%";
		$role = "%" . $this->db->escape_string($role) . "%";
		$status = "%" . $this->db->escape_string($status) . "%";
		$birthname = "%" . $this->db->escape_string($birthname) . "%";
		if ($notNull) {
			$statusCheck = "AND status NOT LIKE " . self::$status_dead;
		}
		$sql = "SELECT * FROM " . self::$table_alumni .
				" WHERE " .
					"lastname LIKE '$lastname' " .
					"AND firstname LIKE '$firstname' " .
					"AND mail LIKE '$mail' " .
					"AND agegroup LIKE '$agegroup' " .
					"AND birthday LIKE '$birthday' " .
					"AND role LIKE '$role' " .
					"AND status LIKE '$status' " .
					"AND birthname LIKE '$birthname' " .
					$statusCheck .
					"ORDER BY lastname ASC";
		return $this->db->query($sql);
	}

	/**
	 * Check if entry already exists.
	 * Return true if entry already exists.
	 * Return false if not.
	 */
	public function checkDouble($mail) {
		$mail = $this->db->escape_string($mail);
		$sql = "SELECT mail FROM " . self::$table_alumni .
				" WHERE" .
					" mail LIKE '$mail' AND" .
					" status NOT LIKE " . self::$status_dead .
				" LIMIT 1";
		$results = $this->db->query($sql)->num_rows;
		return $results > 0;
	}
}
?>
