<?php
/*
 * @author	Nico Alt
 * @date	28.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
class Newsletter {

	// Utilities
	private $utils;

	// Connection to database
	private $db;

	/**
	 * Connect with the database.
	 */
	public function __construct() {
		require_once __DIR__ . '/utilities.php';
		$this->utils = new Utilities();

		require_once __DIR__ . '/database.php';
		$database = new Database();
		$this->db = $database->get();

		require_once __DIR__ . '/configuration.php';
	}

	/**
	 * Send newsletter
	 */
	public function send($token) {
		// Get addresses
		$sql = "SELECT mail FROM " . Database::$table_alumni .
				" WHERE" .
					" status = " . Database::$status_active;
		$query = $this->db->query($sql);
		if (!$query || $query->num_rows == 0) {
			return false;
		}
		$addresses = array();
		while($column = $query->fetch_array()) {
			$addresses[] = $column['mail'];
		}

		// Build newsletter
		$sql = "SELECT * FROM " . Database::$table_newsletter . " WHERE token = '$token' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows == 0) {
			return false;
		}
		$row = $query->fetch_array();
		$newsletter = $row['newsletter'];
		$subject = $row['subject'];

		$headers = $this->utils->getHeaders(null, null, null);

		// Send newsletter to addresses
		foreach ($addresses as $address) {
			if (mail($address, $subject, $newsletter, $headers)) {
				$log .= "Successfully sent to \"" . $address . "\"" . "\r\n";
			}
			else {
				$log .= "Could not sent to \"" . $address . "\"" . "\r\n";
			}
		}

		// Send log to Alumni-Team
		$to = Configuration::get("Newsletter", "Log_Recipient_Address");
		$subject = "Log of Newsletter \"" . $subject . "\"";
		$log .= "\r\n" . "\r\n" .
				"==============================" .
				"\r\n" . "\r\n" .
				$newsletter;
		mail($to, $subject, $log, $headers);

		// Mark newsletter as sent
		$log = $this->db->escape_string($log);
		$token = $this->db->escape_string($token);
		$sql = "UPDATE " . Database::$table_newsletter .
				" SET
					sent = 1,
					log = '$log'
				WHERE
					token = '$token' LIMIT 1";
		return $this->db->query($sql);
	}

	/**
	 * Send a test mail with the newsletter
	 */
	public function sendTest($subject, $mail, $newsletter) {
		$subject = $this->db->escape_string($subject);
		$mail = $this->db->escape_string($mail);
		$newsletter = $this->db->escape_string($newsletter);
		$token = bin2hex(openssl_random_pseudo_bytes(16));

		$sql = "INSERT INTO " . Database::$table_newsletter .
				" (
					token,
					subject,
					newsletter
				)" .
				" VALUES (
					'$token',
					'$subject',
					'$newsletter'
				)";
		if (!$this->db->query($sql)) {
			echo(mysqli_error($this->db));
			return false;
		}

		// Build newsletter
		$sql = "SELECT * FROM " . Database::$table_newsletter . " WHERE token = '$token' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows == 0) {
			return false;
		}
		$row = $query->fetch_array();
		$newsletter = $row['newsletter'];
		$subject = $row['subject'];

		$newsletter = $newsletter . "\r\n" . "\r\n" .
					"==============================" . "\r\n" . "\r\n" .
					"Wenn Du den Newsletter versenden möchtest, klicke bitte hier:" . "\r\n" .
					Configuration::get("Website", "URL_Root") . "/admin/confirm.php?t=$token";

		$headers = $this->utils->getHeaders(null, null, null);

		// Send mail
		return mail($mail, $subject, $newsletter, $headers);
	}
}
?>
