<?php
/*
 * @author	Nico Alt
 * @date	28.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
class Confirmation {

	// Connection to database
	private $db;

	/**
	 * Connect with the database.
	 */
	public function __construct() {
		require_once __DIR__ . '/database.php';
		$database = new Database();
		$this->db = $database->get();

		require_once __DIR__ . '/configuration.php';
	}

	/**
	 * Send confirmation mail.
	 */
	public function send_User($mail) {
		$mail = $this->db->escape_string($mail);
		$sql = "SELECT * FROM " . Database::$table_alumni .
				" WHERE".
					" mail LIKE '$mail' AND" .
					" status NOT LIKE " . Database::$status_dead .
				" LIMIT 1";
		$row = $this->db->query($sql)->fetch_array();
		$token = $row['token'];
		$status = $row['status'];
		$firstname = $row['firstname'];
		$lastname = $row['lastname'];

		$subject = Configuration::get("Confirmation", "Subject");
		$school = Configuration::get("General", "Title_School");
		$url_root = Configuration::get("Website", "URL_Root");

		require_once __DIR__ . '/utilities.php';
		$utils = new Utilities();
		$headers = $utils->getHeaders(null, null, null);
		if ($status == Database::$status_toGetConfirmed) {
			$message = "Sehr geehrte(r) $firstname $lastname," . "\r\n" . "\r\n" .
						"Sie haben sich mit der E-Mail-Adresse \"$mail\" für den Newsletter der Schule \"" . $school . "\" eingetragen." . "\r\n" . "\r\n" .
						"Klicken Sie auf den folgenden Link, um Ihre E-Mail-Adresse zu bestätigen:" . "\r\n" .
						$url_root . "/confirm.php?t=$token" . "\r\n" . "\r\n" .
						"Falls Sie sich nicht in den Alumni-Newsletter der Schule eingetragen haben, ignorieren Sie diese E-Mail einfach." . "\r\n" . "\r\n" .
						"Mit freundlichen Grüßen," . "\r\n" . "\r\n" .
						"Das Newsletter-Team";

			return mail($mail, $subject, $message, $headers);
		}
		if ($status == Database::$status_toGetDeleted) {
			$message = "Sehr geehrte(r) $firstname $lastname," . "\r\n" . "\r\n" .
						"Sie erhalten diese E-Mail, weil angegeben wurde, dass Sie keine Newsletter der Schule \"" . $school . "\" erhalten möchten." . "\r\n" . "\r\n" .
						"Wenn dem so ist, klicken Sie auf den folgenden Link, um den Austragewunsch zu bestätigen:" . "\r\n" .
						$url_root . "/confirm.php?t=$token" . "\r\n" . "\r\n" .
						"Falls Sie sich nicht aus dem Alumni-Newsletter der Schule austragen möchten, ignorieren Sie diese E-Mail einfach." . "\r\n" . "\r\n" .
						"Mit freundlichen Grüßen," . "\r\n" . "\r\n" .
						"Das Newsletter-Team";

			return mail($mail, $subject, $message, $headers);
		}
		return false;
	}

	/**
	 * Confirm a user.
	 * Returns 0 if confirmation was not successful.
	 * Returns 1 if user is signed in.
	 * Returns 2 if user is signed out.
	 */
	public function confirm_User($token) {
		$token = $this->db->escape_string($token);

		// Check if the token is in the database
		$query = $this->db->query("SELECT * FROM " . Database::$table_alumni . " WHERE token = '$token' LIMIT 1");
		if($query->num_rows != 0) {
			$row = $query->fetch_array();
			$mail = $row['mail'];
			$oldStatus = $row['status'];

			$mail = $this->db->escape_string($mail);
			// Want to get in
			if ($oldStatus == Database::$status_toGetConfirmed) {
				if($this->db->query("UPDATE " . Database::$table_alumni .
										" SET" .
											" status = " . Database::$status_active .
										" WHERE" .
											" mail = '$mail' AND" .
											" status NOT LIKE " . Database::$status_dead .
										" LIMIT 1")) {
					return 1;
				}
			}
			// Want to get out
			if ($oldStatus == Database::$status_toGetDeleted) {
				if($this->db->query("UPDATE " . Database::$table_alumni .
										" SET" .
											" status = " . Database::$status_dead .
										" WHERE" .
											" mail = '$mail' AND" .
											" status NOT LIKE " . Database::$status_dead .
										" LIMIT 1")) {
					return 2;
				}
			}
		}
		return 0;
	}

	/**
	 * Confirm an action made by an admin.
	 * Returns 0 if confirmation was not successful.
	 * Returns 1 if a newsletter was confirmed.
	 */
	public function confirm_Admin($token) {
		$token = $this->db->escape_string($token);

		// Check if the token is in the database
		$query = $this->db->query("SELECT sent FROM " . Database::$table_newsletter . " WHERE token = '$token' LIMIT 1");
		if($query->num_rows != 0) {
			$row = $query->fetch_array();
			$sent = $row['sent'];
			if ($sent == '0') {
				return 1;
			}
		}
		return 0;
	}
}
?>
