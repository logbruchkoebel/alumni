<?php
/*
 * @author	Nico Alt
 * @date	21.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
$lastname = $_POST['lastname'];
$firstname = $_POST['firstname'];
$mail = $_POST['mail'];
$message = $_POST['message'];

try {
	// Check if strings are "POSTed"
	if (empty($lastname) && empty($firstname) && empty($mail) && empty($message)) {
		throw new Exception();
	}
	// Check if strings are not empty
	if ($lastname == '' || $firstname == '' || $mail == '' || $message == '') {
		throw new Exception("Oh, da hast Du anscheinend was vergessen... Bitte überprüfe, ob Du alle Felder ausgefült haben.");
	}

	require 'lib/utilities.php';
	$utils = new Utilities();

	// Check if mail is valid
	if (!$utils->checkMail($mail)) {
		throw new Exception("Nein, das geht nicht. Die eingegebene E-Mail-Adresse existiert nicht.");
	}

	require 'lib/contact.php';
	$contact = new Contact();

	// Send mail
	if (!$contact->send_User($firstname, $lastname, $mail, $message)) {
		throw new Exception("Ups, Deine Nachricht konnte leider nicht versandt werden. Bitte versuche es später erneut.");
	}
	// Print out message with details
	$msg = "Yey, Deine Nachricht wurde an das Newsletter-Team versandt.";
}
catch (Exception $e) {
	$msg = $e->getMessage();
}

// Show header
require 'lib/layout.php';
$layout = new Layout();
echo $layout->header("Kontakt", 4, false, "");
?>
<p><b><?=$msg?></b></p>
<p>Kontaktiere das Newsletter-Team:</p>
<form method="post" accept-charset="UTF-8">
	<p><input type="text" name="lastname" placeholder="Name *" value="<?=$lastname?>" size="50"/></p>
	<p><input type="text" name="firstname" placeholder="Vorname *" value="<?=$firstname?>" size="50"/></p>
	<p><input type="email" name="mail" placeholder="E-Mail *" value="<?=$mail?>" size="50"/></p>
	<p><textarea type="text" name="message" placeholder="Text *" cols="64" rows="15"><?=$message?></textarea></p>
	<p><input type="submit" /></p>
</form>
<?
// Show footer
echo $layout->footer("");
?>
