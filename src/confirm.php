<?php
/*
 * @author	Nico Alt
 * @date	21.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
$token = $_GET['t'];

try {
	require 'lib/confirmation.php';
	$confirmation = new Confirmation();

	// Confirm action
	$value = $confirmation->confirm_User($token);
	switch ($value) {
		case 1:
			throw new Exception("Hey, auch Du empfängst von nun an den Newsletter.");
			break;
		case 2:
			throw new Exception("Schade, dass Du Dich abgemeldet hast. Du erhälst von nun an keinen Newsletter mehr.");
			break;
		default:
			$msg = "Es gab einen Fehler. Bitte versuche es später erneut.";
	}
}
catch (Exception $e) {
	$msg = $e->getMessage();
}

// Show header
require 'lib/layout.php';
$layout = new Layout();
echo $layout->header("Bestätigung", 0, false, "");
?>
<p><b><?=$msg?></b></p>
<?
// Show footer
echo $layout->footer("");
?>
