<?php
/*
 * @author	Nico Alt
 * @date	21.10.2015
 *
 * See the file "LICENSE" for the full license governing this code.
 */
// Get string
$mail = $_POST['mail'];

try {
	// Check if strings are "POSTed"
	if (empty($mail)) {
		throw new Exception();
	}

	// Check if strings are not empty
	if ($mail == '') {
		throw new Exception("Oh, da hast Du anscheinend was vergessen... Bitte überprüfe, ob Du alle Felder ausgefült haben.");
	}

	require 'lib/utilities.php';
	$utils = new Utilities();

	// Check if mail is valid
	if (!$utils->checkMail($mail)) {
		throw new Exception("Nein, das geht nicht. Die eingegebene E-Mail-Adresse existiert nicht.");
	}

	require 'lib/database.php';
	$database = new Database();

	// Check if entry already exists
	if (!$database->checkDouble($mail)) {
		throw new Exception("Die eingegebene E-Mail-Adresse ist nicht eingetragen.");
	}

	// Sign out
	if (!$database->signOut($mail, false)) {
		throw new Exception("Ich kann Dich gerade nicht austragen. Bitte versuche es später erneut.");
	}

	require 'lib/confirmation.php';
	$confirmation = new Confirmation();

	// Send confirmation mail
	if (!$confirmation->send_User($mail)) {
		$database->resignIn($mail);
		throw new Exception("Die Bestätigungsmail konnte nicht abgesandt werden. Bitte versuche es später erneut.");
	}

	// Print out message with details
	$msg = "Schade, aber wir haben Dir eine Bestätigungsmail an die Adresse <i>\"$mail\"</i> geschickt. Bitte bestätige Deine Adresse.";
}
catch (Exception $e) {
	$msg = $e->getMessage();
}

// Show header
require 'lib/layout.php';
$layout = new Layout();
echo $layout->header("Austragen", 3, false, "");
?>
<p><b><?=$msg?></b></p>
<p>Trage Dich aus der Datenbank aus:</p>
<form method="post" accept-charset="UTF-8">
	<p><input type="text" name="mail" placeholder="E-Mail *" value="<?=$mail?>"/></p>
	<p><input type="submit" /></p>
</form>
<?
// Show footer
echo $layout->footer("");
?>
